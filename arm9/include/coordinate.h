/*
 *  Coordinate.h
 *  
 *	Simple struct for storing an x and y location on the touch screen.
 *
 *  Created by Jaeden Amero on 7/28/05.
 *  Copyright 2005 PataterSoft. All rights reserved.
 *
 */

#ifndef COORDINATE_INCLUDE
#define COORDINATE_INCLUDE

#include "nds.h"
#include "vector.h"

class Coordinate {

  float xVal;
  float yVal;

 public:

  Coordinate(int x, int y);
  Coordinate(float x = 0.0, float y = 0.0);
 
  int x();
  int y();
  void x(int);
  void y(int);
  void x(float);
  void y(float);
  void moveBy(int, int);
  void moveBy(float, float);
  void moveBy(Vector);
  void convertToTilePosition();
  void clipX(int, int);
  void clipY(int, int);
  bool outOfBoundsX(int, int);
  bool outOfBoundsY(int, int);
  Vector vectorDifference(Coordinate);
  

 private:
  float clip(float, float, float);
  bool outOfBounds(float, float, float);
};

#endif
