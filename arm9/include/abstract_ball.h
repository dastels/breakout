#ifndef ABSTRACT_BALL_INCLUDE
#define ABSTRACT_BALL_INCLUDE

#include "coordinate.h"
#include "rectangle.h"
#include "vector.h"
#include "abstract_cell.h"

class AbstractBall {
 public:

    virtual Vector getVelocity() = 0;
    virtual void addToVelocity(Vector v) = 0;
    virtual void subtractFromVelocity(Vector v) = 0;
    virtual void move() = 0;
    virtual bool isHeadingLeft() = 0;
    virtual bool isHeadingRight() = 0;
    virtual bool isHeadingUp() = 0;
    virtual bool isHeadingDown() = 0;
    virtual bool isHeadingPrimarilyLeft() = 0;
    virtual bool isHeadingPrimarilyRight() = 0;
    virtual bool isHeadingPrimarilyUp() = 0;
    virtual bool isHeadingPrimarilyDown() = 0;
    virtual Coordinate center() = 0;
    virtual Coordinate getPosition() = 0;
    virtual Coordinate getLeftCellPosition() = 0;
    virtual Coordinate getRightCellPosition() = 0;
    virtual Coordinate getTopCellPosition() = 0;
    virtual Coordinate getBottomCellPosition() = 0;
    virtual void reflectFromSide(AbstractCell *) = 0;
    virtual void reflectFromTopBottom(AbstractCell *) = 0;
    virtual Rectangle *boundingBox() = 0;
    virtual bool isCollidingWith(Rectangle *) = 0;
    virtual bool isCollidingWith(AbstractBall *) = 0;
    virtual Vector transferredVelocity() = 0;
    virtual ~AbstractBall() {}
};

#endif
