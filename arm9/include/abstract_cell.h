#ifndef ABSTRACT_CELL_INCLUDE
#define ABSTRACT_CELL_INCLUDE

#include "rectangle.h"

class AbstractBall;

class AbstractCell {
 public:
  virtual int tileNumber() = 0;
  virtual int getRow() = 0;
  virtual int getColumn() = 0;
  virtual bool isHorizontal() = 0;
  virtual bool isVertical() = 0;
  virtual bool isHitBy(AbstractBall *b) = 0;
  virtual void processHit(AbstractBall *b) = 0;
  virtual void reflectOffVertical(Vector&) = 0;
  virtual void reflectOffHorizontal(Vector&) = 0;
  virtual Rectangle *boundingBox() = 0;
  virtual ~AbstractCell() {};
};

#endif
