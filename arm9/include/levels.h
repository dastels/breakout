#ifndef LEVELS_INCLUDE
#define LEVELS_INCLUDE

char *levels[] = 
  {
    // level 1
    "                              ", //  0
    "                              ", //  1
    "GGGGGGGGGGGGGGGGGGGGGGGGGGGGGG", //  2
    "GGGGGGGGGGGGGGGGGGGGGGGGGGGGGG", //  3
    "                              ", //  4
    "    GG  GG  GG  GG  GG  GG    ", //  5
    "  BB  BB  BB  BB  BB  BB  BB  ", //  6
    "    GG  GG  GG  GG  GG  GG    ", //  7
    "                              ", //  8
    "                              ", //  9
    "BBBBBBBBBoBBBBBBBBBBoBBBBBBBBB", // 10
    "RRRRRRRRRRRRRRRRRRRRRRRRRRRRRR", // 11
    "                              ", // 12
    "                              ", // 13
    "   SS    S    SS    S    SS   ", // 14
    "                              ", // 15
    "                              ", // 16
    "                              ", // 17
    "                              ", // 18
    "                              ", // 19

    "SSSS                      SSSS", //  0
    "SS                          SS", //  1
    "S        BBBBBBBBBBBB        S", //  2
    "S       BBRRRRRRRRRRBB       S", //  3
    "       BBRR  GGGG  RRBB       ", //  4
    "      BBRR GGGooGGG RRBB      ", //  5
    "       BBRR  GGGG  RRBB       ", //  6
    "S       BBRRRRRRRRR BB       S", //  7
    "S        BBBBBBBBBBBB        S", //  8
    "SS                          SS", //  9
    "SSSS                      SSSS", // 10
    "BBBBBBBBBBBBBBBBBBBBBBBBBBBBBB", // 11
    "BBBB  BBB  BBB  BBB  BBB  BBBB", // 12
    "       B    B    B    B       ", // 13
    "                              ", // 14
    "                              ", // 15
    "                              ", // 16
    "                              ", // 17
    "                              ", // 18
    "                              ", // 19

    "GGGGGGGGGGGGGGGGGGGGGGGGGGGGGG", //  0
    "GGGGGGGGGGGGGGGGGGGGGGGGGGGGGG", //  1
    "                              ", //  2
    "S   S   S   S    S   S   S   S", //  3
    "                              ", //  4
    "RRRRRRRRRRRRRRRRRRRRRRRRRRRRRR", //  5
    "                              ", //  6
    "  S       S        S       S  ", //  7
    "                              ", //  8
    "BBBBBBBBBBBBBBBBBBBBBBBBBBBBBB", //  9
    "                              ", // 10
    "S   S   S   S    S   S   S   S", // 11
    "                              ", // 12
    "                              ", // 13
    "                              ", // 14
    "                              ", // 15
    "                              ", // 16
    "                              ", // 17
    "                              ", // 18
    "                              ", // 19

    "                              ", //  0
    "                              ", //  1
    "                              ", //  2
    "       ooo   ooo   ooo        ", //  3
    "          ooo   ooo           ", //  4
    "                              ", //  5
    "                              ", //  6
    "  S       S        S       S  ", //  7
    "                              ", //  8
    "                              ", //  9
    "          ooo   ooo           ", // 10
    "       ooo   ooo   ooo        ", // 11
    "                              ", // 12
    "                              ", // 13
    "                              ", // 14
    "                              ", // 15
    "                              ", // 16
    "                              ", // 17
    "                              ", // 18
    "SSSSSSSSSSS      SSSSSSSSSSSSS", // 19

      // more levels
  };

#endif

