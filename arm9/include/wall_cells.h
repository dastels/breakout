#ifndef WALL_CELLS_INCLUDE
#define WALL_CELLS_INCLUDE

#include "cell.h"

class WallCell: public Cell {
  Rectangle bounds;

 public:
  WallCell(int top, int left, int bottom, int right);
  Rectangle *boundingBox();
};

class SideWallCell: public WallCell {
 public:
  SideWallCell(bool onRight);
  bool isHorizontal();
};

class TopWallCell: public WallCell {
 public:
  TopWallCell();
  bool isVertical();
};

#endif
