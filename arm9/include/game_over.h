char *gameOver[] = 
  {
    "                                ", // 0
    "                                ", 
    "                                ", 
    "                                ",
    "    BBB    BBB   B   B  BBBBB   ",
    "   B   B  B   B  BB BB  B       ", // 5
    "   G      G   G  G G G  GGGG    ",
    "   G  GG  GGGGG  G G G  G       ", 
    "   R   R  R   R  R   R  R       ",
    "    RRR   R   R  R   R  RRRRR   ",
    "                                ", // 10
    "                                ",
    "    BBB   B   B  BBBBB  BBBB    ",
    "   B   B  B   B  B      B   B   ",
    "   G   G   G G   GGGG   G   G   ",
    "   G   G   G G   G      GGGG    ", // 15
    "   R   R   R R   R      R   R   ",
    "    RRR     R    RRRRR  R   R   ", 
    "                                ",
    "                                ",
    "                                ", // 20
    "                                ",
    "                                ", 
    "                                "
  };
