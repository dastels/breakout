#ifndef BLOCK_CELLS_INCLUDE
#define BLOCK_CELLS_INCLUDE

#include "nds.h"
#include "cell.h"

class BlockCell:
public Cell
{
    Rectangle bounds;
    
  public:
    static AbstractCell* createCell(Board *theBoard, char type, int row, int column);
    static int codeToTile(char);
    static BlockCell** sampleBlocks();
    
    BlockCell(Board* theBoard, int row, int column);
    virtual int value();
    virtual int tileNumber();
    virtual TransferSoundData *sound();
    virtual bool removable();
        
    void processHit(AbstractBall *b);
    Rectangle *boundingBox();
};

//----------------------------------------

class RedBlockCell:
public BlockCell
{
    static TransferSoundData *sound_data;
    
  public:
    RedBlockCell(Board* theBoard, int row, int column);
    int tileNumber();
    int value();
    TransferSoundData *sound();
};

//----------------------------------------

class BlueBlockCell:
public BlockCell
{
    static TransferSoundData *sound_data;
  public:
    BlueBlockCell(Board* theBoard, int row, int column);
    int tileNumber();
    int value();
    TransferSoundData *sound();
};

//----------------------------------------

class GreenBlockCell:
public BlockCell
{
    static TransferSoundData *sound_data;
  public:
    GreenBlockCell(Board* theBoard, int row, int column);
    int tileNumber();
    int value();
    TransferSoundData *sound();
};

//----------------------------------------

class SolidBlockCell:
public BlockCell
{
    static TransferSoundData *sound_data;
  public:
    SolidBlockCell(Board *theBoard, int row, int column);
    int tileNumber();
    int value();
    TransferSoundData *sound();
    bool removable();
};
    
//----------------------------------------

class BallBlockCell:
public BlockCell 
{
    static TransferSoundData *sound_data;
  public:
    BallBlockCell(Board *theBoard, int row, int column);
    int tileNumber();
    int value();
    TransferSoundData *sound();
    void processHit(AbstractBall *b);
    bool removable();
};


#endif
