#ifndef TEXT_INCLUDE
#define TEXT_INCLUDE

#include <nds.h>

class Text {
  uint16 *screenRam;
  int currentPosition;
  
 public:
  Text(uint16*);
  void moveTo(int row, int column);
  void put(char);
  void put(int row, int column, char c);
  void puts(int row, int column, char *s);
  void puts(char *s);
};

#endif
