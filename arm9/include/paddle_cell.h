#ifndef PADDLE_CELL_INCLUDE
#define PADDLE_CELL_INCLUDE

#include "cell.h"
#include "nds.h"

class PaddleCell: public Cell {
  static TransferSoundData *paddle_sound;
  Rectangle bounds;
  Paddle *paddle;
  int x;

 public:
  PaddleCell(Board *theBoard, Paddle *pdl);
  Rectangle *boundingBox();
  void processHit(AbstractBall *b);
  bool isVertical();
  void reflectOffHorizontal(Vector&);
};

#endif
