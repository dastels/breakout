char *brick[] = 
  {
    "                                ", // 0
    "   BBB   BBB   BBB   BB   B  B  ", 
    "   B  B  B  B   B   B  B  B B   ", 
    "   BBB   BBB    B   B     BB    ",
    "   B  B  B  B   B   B     B B   ",
    "   B  B  B  B   B   B  B  B  B  ", // 5
    "   BBB   B  B  BBB   BB   B  B  ",
    "                                ", 
    "                                ",
    "                                ",
    "                                ", // 10
    "                                ",
    "                                ",
    "                                ",
    "                                ",
    "                                ", // 15
    "                                ",
    "                                ", 
    "                                ",
    "                                ",
    "                                ", // 20
    "                                ",
    "                                ", 
    "                                "
  };
