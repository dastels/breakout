#ifndef BREAKOUT_INCLUDE
#define BREAKOUT_INCLUDE

#include "nds.h"

// some colours

#define WHITE RGB15(31, 31, 31)
#define BLACK RGB15(0, 0, 0)

#define min(a, b) (a < b) ? a : b
#define max(a, b) (a > b) ? a : b

void displayScore(uint16*, int);
void displayBallsRemaining(uint16*, int);

#endif
