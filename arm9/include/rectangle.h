#ifndef RECTANGLE_INCLUDE
#define RECTANGLE_INCLUDE

#include "coordinate.h"

class Rectangle {
  static Coordinate coord;

 public:
  int top;
  int left;
  int bottom;
  int right;

  static Rectangle *create(int t, int l, int b, int r);
  Rectangle(int t, int l, int b, int r);
  virtual bool isEmpty();
  virtual Rectangle *operator|(Rectangle*);
  virtual Rectangle *operator&(Rectangle*);
  virtual Rectangle *inset(int);
  virtual bool collidesWith(Rectangle*);
  virtual ~Rectangle();

  // perimeter coords
  Coordinate bottomLeft();
  Coordinate bottomCenter();
  Coordinate bottomRight();
  Coordinate topLeft();
  Coordinate topCenter();
  Coordinate topRight();
  Coordinate middleLeft();
  Coordinate middleRight();

  Coordinate center();
};

class EmptyRectangle : public Rectangle {
 public:
  EmptyRectangle();
  bool isEmpty();
  Rectangle *operator|(Rectangle*);
  Rectangle *operator&(Rectangle*);
  Rectangle *inset(int);
  bool collidesWith(Rectangle*);
};

#endif
