#ifndef BALL_INCLUDE
#define BALL_INCLUDE

#include "coordinate.h"
#include "abstract_ball.h"
#include "abstract_cell.h"
#include "board.h"
#include "vector.h"

class Ball: public AbstractBall {
    Coordinate position;
    Coordinate cellPosition;
    Rectangle collisionBoundingBox;
    Vector velocity;
    Board *theBoard;
    
    void updateBoundingBox();
    
  public:
    
    Ball(Board*);
    Ball(Board *aBoard, int x, int y, Vector initialVelocity);
    void reset();

    Vector getVelocity();
    void addToVelocity(Vector v);
    void subtractFromVelocity(Vector v);

    void move();
    bool isHeadingLeft() { return velocity.isLeft(); }
    bool isHeadingRight() { return velocity.isRight(); }
    bool isHeadingUp() { return velocity.isUp(); }
    bool isHeadingDown() { return velocity.isDown(); }
    bool isHeadingPrimarilyLeft() { return velocity.isPrimarilyLeft(); }
    bool isHeadingPrimarilyRight() { return velocity.isPrimarilyRight(); }
    bool isHeadingPrimarilyUp() { return velocity.isPrimarilyUp(); }
    bool isHeadingPrimarilyDown() { return velocity.isPrimarilyDown(); }

    Coordinate center();
    Coordinate getPosition();
    Coordinate getLeftCellPosition() { return convertToTile(collisionBoundingBox.middleLeft()); }
    Coordinate getRightCellPosition() { return convertToTile(collisionBoundingBox.middleRight()); }
    Coordinate getTopCellPosition() { return convertToTile(collisionBoundingBox.topCenter()); }
    Coordinate getBottomCellPosition() { return convertToTile(collisionBoundingBox.bottomCenter()); }
    void reflectFromSide(AbstractCell *);
    void reflectFromTopBottom(AbstractCell *);

    Rectangle *boundingBox();
    bool isCollidingWith(Rectangle *box);
    bool isCollidingWith(AbstractBall *aBall);
    Vector transferredVelocity();
    Coordinate convertToTile(Coordinate c);
};

#endif
