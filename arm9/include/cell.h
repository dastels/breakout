#ifndef CELL_INCLUDE
#define CELL_INCLUDE

#include "abstract_cell.h"
#include "board.h"

class Cell: public AbstractCell {
 protected:
  Board *board;
  int row;
  int column;

 public:
  int getRow();
  int getColumn();
  int tileNumber();
  bool isHorizontal();
  bool isVertical();

  Cell(Board *theBoard, int c, int r);
  bool isHitBy(AbstractBall *b);
  void processHit(AbstractBall *b);
  void reflectOffVertical(Vector&);
  void reflectOffHorizontal(Vector&);
  ~Cell();
};

typedef Cell *CellPtr;

#endif
