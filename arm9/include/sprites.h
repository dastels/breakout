#ifndef SPRITES_INCLUDE
#define SPRITES_INCLUDE

void updateOAM(SpriteEntry*);
void initOAM(SpriteEntry*, SpriteRotation*);
void moveSprite(SpriteEntry*, u16, u16);
void rotateSprite(SpriteRotation *, u16);
void enableSprite(SpriteEntry *sprites, int spriteToEnable);
void disableSprite(SpriteEntry *sprites, int spriteToDisable);
#endif
