#ifndef PADDLE_INCLUDE
#define PADDLE_INCLUDE

#include "coordinate.h"
#include "vector.h"

class Paddle {
    Coordinate position;
    Vector *velocity;
    
    
  public:
    Paddle();
    void moveLeft();
    void moveRight();
    void stop();
    
    Coordinate getPosition();
    void addVelocityTo(Vector&);
};

#endif
