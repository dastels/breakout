#ifndef EMPTY_CELL_INCLUDE
#define EMPTY_CELL_INCLUDE

#include "cell.h"

class EmptyCell: public Cell {
  Rectangle *emptyBox;

 public:
  EmptyCell();
  bool isHitBy(AbstractBall *b);
  Rectangle *boundingBox();
};

#endif
