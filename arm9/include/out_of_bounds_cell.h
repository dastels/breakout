#ifndef OUT_OF_BOUNDS_CELL_INCLUDE
#define OUT_OF_BOUNDS_CELL_INCLUDE

#include "cell.h"

class OutOfBoundsCell: public Cell {
  static Rectangle bounds;

 public:
  OutOfBoundsCell(Board*);
  Rectangle *boundingBox();
  void processHit(AbstractBall*);
};

#endif
