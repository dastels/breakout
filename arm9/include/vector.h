#ifndef VECTOR_INCLUDE
#define VECTOR_INCLUDE

#define TWO_PI (6.283185)
#define PI (3.1415927)
#define HALF_PI (1.570796)
#define QUARTER_PI (0.785398)

class Vector {
    float angle;
    float speed;

  public:
    Vector(float a = 0.0, float v = 0.0);
    float x();
    float y();
    float getAngle();
    float magnitude();
    int angleAsDeg512();

    char* to_s();   

    void operator+=(Vector);
    Vector operator+(Vector);
    void operator-=(Vector);
    Vector operator-(Vector);
    void operator/=(float);
    Vector operator/(float);
    void operator*=(float);
    Vector operator*(float);
    float dot(Vector other);
    void normalize();
    void normalizeAngle();
    void flipX();
    void flipY();
    void clipMagnitudeAt(float max);
    void clipMagnitudeBetween(float min, float max);
    void clipAngleBetween(float mix, float max);

    bool isLeft();
    bool isRight();
    bool isUp();
    bool isDown();
    bool isPrimarilyLeft();
    bool isPrimarilyRight();
    bool isPrimarilyUp();
    bool isPrimarilyDown();
    bool isInTheSameDirectionAs(Vector v);
};



#endif
