#ifndef BOARD_INCLUDE
#define BOARD_INCLUDE

#include "abstract_ball.h"
#include "abstract_cell.h"
#include "paddle.h"
#include "text.h"

#define MAX_NUMBER_OF_BALLS_IN_PLAY 8

class Board {
  uint16 *screen;
  uint16 *subScreen;
  SpriteEntry *sprites;
  
  Paddle *paddle;
  AbstractCell *cells[25][32];
  AbstractBall *balls[MAX_NUMBER_OF_BALLS_IN_PLAY];
  int number_of_balls;
  bool inPlay;
  int ballsRemaining;
  int score;
  Text textWriter;

 public:
  Board(Paddle*, uint16*, uint16*, SpriteEntry*);
  void resetGame();
  AbstractCell *cellAt(int, int);
  AbstractCell *cellAt(Coordinate);
  void processBall(AbstractBall*);
  void wentOutOfBounds(AbstractBall*);
  bool isStillInPlay();
  bool isGameOver();
  void addToScore(int value);
  void updateScoreDisplay();
  void updateBallsRemaining();
  void removeBlock(AbstractCell*);
  int paddleX();
  void clearLevel();
  void setupLevel(int);
  void displayScreen(char *data[], uint16 *destination);
  void displayGameOver();
  void moveBalls();
  void launch();
  bool canLaunchAnotherBall();
  void addBall(int row, int column, Vector initialVelocity);
  
 private:
  AbstractCell *createCell(char type, int row, int column);
  void addBorder();
  void addPaddle(Paddle*);
  void addOutOfBounds();
  void addBlocks();
  void clearBalls();
  void addAndEnableBall(AbstractBall*);

  void checkForAndHandleCollision(AbstractBall*);

  bool checkForAndHandleBallCollision(AbstractBall *aBall);

  bool checkForAndHandleCornerCollision(AbstractBall*);
  bool verticalHit(AbstractCell *aCell, AbstractBall *aBall);
  bool horizontalHit(AbstractCell *aCell, AbstractBall *aBall);
  
  bool checkForAndHandleNonCornerCollision(AbstractBall*);
  bool checkForAndHandleLeftHit(AbstractCell*, AbstractBall*);
  bool checkForAndHandleRightHit(AbstractCell*, AbstractBall*);
  bool checkForAndHandleTopHit(AbstractCell*, AbstractBall*);
  bool checkForAndHandleBottomHit(AbstractCell*, AbstractBall*);
};

#endif
