#include "paddle_cell.h"
#include "paddle_1_11025_8_s_raw.h"

  
TransferSoundData *PaddleCell::paddle_sound = 0;

PaddleCell::PaddleCell(Board *theBoard, Paddle *pdl)
  : Cell(theBoard, 0, 0), bounds(SCREEN_HEIGHT - 2, 0, SCREEN_HEIGHT - 1, 0)
{
  paddle = pdl;

  if (paddle_sound) return;

  paddle_sound = new TransferSoundData();
  paddle_sound->data = paddle_1_11025_8_s_raw;
  paddle_sound->len = paddle_1_11025_8_s_raw_size;
  paddle_sound->rate = 11025;
  paddle_sound->vol = 127;
  paddle_sound->pan = 64;
  paddle_sound->format = 1;
}

Rectangle *PaddleCell::boundingBox()
{
  bounds.left = paddle->getPosition().x();
  bounds.right = bounds.left + 31;
  return &bounds;
}

void PaddleCell::processHit(AbstractBall *b)
{
    Cell::processHit(b);
    playSound(paddle_sound);
}

bool PaddleCell::isVertical()
{
    return false;
}

void PaddleCell::reflectOffHorizontal(Vector &v)
{
    paddle->addVelocityTo(v);
    
    v.flipY();
}

