#include "text.h"

#define ZERO_TILE 32
#define FONT_BASE 16
#define FIRST_CHAR 32

Text::Text(uint16*address)
{
  screenRam = address;
  currentPosition = 0;
}

void Text::moveTo(int row, int column)
{
  currentPosition = row * 32 + column;
}

void Text::put(char c)
{
  screenRam[currentPosition++] = FONT_BASE + c - FIRST_CHAR;
}

void Text::put(int row, int column, char c)
{
  moveTo(row, column);
  put(c);
}

void Text::puts(char *s)
{
  while (*s)
    put(*s++);
}

void Text::puts(int row, int column, char *s)
{
  moveTo(row, column);
  puts(s);
}
