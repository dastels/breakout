#include <math.h>
#include "vector.h"
#include "stdio.h"

Vector::Vector(float a, float v)
{
    angle = a;
    speed = v;
}

char* Vector::to_s()
{
    char *buffer = new char(20);
    sprintf(buffer, "a: %6.4f, s: %4.2f", angle, speed);
    return buffer;
}

float Vector::x()
{
    return speed * cos(angle);
}

float Vector::y()
{
    return speed * sin(angle);
}

float Vector::getAngle()
{
    return angle;
}

float Vector::magnitude()
{
    return speed;
}

void Vector::flipX()
{
    angle = ((angle > PI) ? TWO_PI : 0.0) + PI - angle;
}

void Vector::flipY()
{
    angle = TWO_PI - angle;
}

int Vector::angleAsDeg512()
{
    return (((int)(angle * 163)) >> 1);
}

void Vector::operator+=(Vector v)
{
    float cx = x() + v.x();
    float cy = y() + v.y();

    angle = atan2(cy, cx);
    speed = sqrt(cx*cx + cy*cy);
}

Vector Vector::operator+(Vector v)
{
    Vector sum = *this;
    sum += v;
    return sum;
}

void Vector::operator-=(Vector v)
{
    float cx = x() - v.x();
    float cy = y() - v.y();

    angle = atan2(cy, cx);
    speed = sqrt(cx*cx + cy*cy);
}

Vector Vector::operator-(Vector v)
{
    Vector sum = *this;
    sum -= v;
    return sum;
}

void Vector::operator/=(float f)
{
    speed /= f;
}
    
void Vector::operator*=(float f)
{
    speed *= f;
}
    
Vector Vector::operator/(float f)
{
    return Vector(angle, speed / f);
}
    
Vector Vector::operator*(float f)
{
    return Vector(angle, speed * f);
}

float Vector::dot(Vector other)
{
    return (x() * other.x()) + (y() * other.y());
}

void Vector::normalize()
{
    speed = 1.0;
}
    
void Vector::clipMagnitudeAt(float max)
{
    if (speed > max) speed = max;
}

void Vector::clipMagnitudeBetween(float min, float max)
{
    if (magnitude < min) magnitude = min;
    if (magnitude > max) magnitude = max;
}

void Vector::clipAngleBetween(float min, float max)
{
    if (angle < min) angle = min;
    if (angle > max) angle = max;
}

void Vector::normalizeAngle()
{
    while (angle > TWO_PI)
        angle -= TWO_PI;
}

bool Vector::isLeft()
{
    normalizeAngle();
    return (angle > HALF_PI) && (angle < PI + HALF_PI);
}

bool Vector::isRight()
{
    normalizeAngle();
    return (angle > PI + HALF_PI) || (angle < HALF_PI);
}

bool Vector::isDown()
{
    normalizeAngle();
    return (angle < PI);
}

bool Vector::isUp()
{
    normalizeAngle();
    return (angle > PI);
}

bool Vector::isPrimarilyRight()
{
    normalizeAngle();
    return (angle <= QUARTER_PI) || (angle >= PI + HALF_PI + QUARTER_PI);
}

bool Vector::isPrimarilyUp()
{
    normalizeAngle();
    return (angle >= PI + QUARTER_PI) && (angle <= PI + HALF_PI + QUARTER_PI);
}

bool Vector::isPrimarilyLeft()
{
    normalizeAngle();
    return (angle >= HALF_PI + QUARTER_PI) && (angle <= PI + QUARTER_PI);
}

bool Vector::isPrimarilyDown()
{
    normalizeAngle();
    return (angle >= QUARTER_PI) && (angle <= HALF_PI + QUARTER_PI);
}

bool Vector::isInTheSameDirectionAs(Vector v)
{
    Vector difference = v - *this;
    return difference.getAngle() < HALF_PI;
}
