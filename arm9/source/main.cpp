/*---------------------------------------------------------------------------------
	Breakout Main file
---------------------------------------------------------------------------------*/
#include <stdlib.h>
#include <stdio.h>
#include <nds.h>

#include "breakout.h"
#include "paddle.h"
#include "ball.h"
#include "sprites.h"
#include "coordinate.h"
#include "board.h"
#include "block_cells.h"

#include "palette_data.h"
#include "paddle_data.h"
#include "tile_data.h"
#include "ball_data.h"
#include "brick.h"

#include "gameover_1_11025_8_s_raw.h"
#include "start_1_11025_8_s_raw.h"

Board *board;
Paddle *paddle;
Text *mainText;
Text *subText;
bool first_ball;
uint current_level;

void initVideo() 
{
    vramSetMainBanks(VRAM_A_MAIN_BG_0x6000000, VRAM_B_MAIN_BG_0x6020000, VRAM_C_SUB_BG_0x6200000, VRAM_D_LCD);
    vramSetBankE(VRAM_E_MAIN_SPRITE);
    videoSetMode(MODE_5_2D | DISPLAY_SPR_ACTIVE | DISPLAY_BG1_ACTIVE | DISPLAY_SPR_1D);
    videoSetModeSub(MODE_5_2D | DISPLAY_BG1_ACTIVE);
}

void initBackgrounds() 
{
    BG1_CR = BG_256_COLOR | BG_32x32 | BG_MAP_BASE(31) | BG_TILE_BASE(0) | BG_PRIORITY(3);
    SUB_BG1_CR = BG_256_COLOR | BG_32x32 | BG_MAP_BASE(31) | BG_TILE_BASE(0) | BG_PRIORITY(3);
}

void loadTiles() 
{
    uint16 *tileRam = (uint16*)(BG_TILE_RAM(0));
    uint16 *subTileRam = (uint16*)(BG_TILE_RAM_SUB(0));
    for (int i = 0; i < (int)sizeof(tileData); i++) {
        tileRam[i] = tileData[i];
        subTileRam[i] = tileData[i];
    }
}

void clearScreen(uint16 *screen)
{
    for (int i = 0; i < 32*24; i++)
        screen[i] = 0;  
}

void displaySubSplashScreen(uint16 *sub_screen)
{
    clearScreen(sub_screen);

    int block_numbers[6*32];

    for (int i = 0; i < 6*32; i++)
        block_numbers[i] = i;

    for (int i = 0; i < 2*6*32; i++) {
        int index1 = rand() % (6*32);
        int index2 = rand() % (6*32);
        
        int temp = block_numbers[index1];
        block_numbers[index1] = block_numbers[index2];
        block_numbers[index2] = temp;
    }

    for (int i = 0; i < 6*32; i++) {
        int index = block_numbers[i];
        int row = index / 32 + 1;
        int col = index % 32;

        swiWaitForVBlank();
        sub_screen[row * 32 + col] = BlockCell::codeToTile(brick[row][col]);
    }
}
    
void displayMainSplashScreen(uint16 *screen)
{
    clearScreen(screen);
    
    mainText->puts(2, 4, "Press START to begin");
    mainText->puts(6, 4, "Launch a ball with A");
    mainText->puts(7, 4, "Move paddle with D-pad");
}

void displayBackground(uint16 *screen) 
{
    for (int i = 0; i < 32*24; i++)
        screen[i] = backgroundData[i];
}


void displayLegend(uint16 *screen)
{
    BlockCell **blocks = BlockCell::sampleBlocks();
    for (int i = 0; blocks[i]; i++) {
        if (blocks[i]->value()) {
            int row = 10 + i % 4;
            int column = (i > 3) ? 18 : 0;
            screen[row * 32 + column] = blocks[i]->tileNumber();
            char buf[13];
            sprintf(buf, "- %3d Points", blocks[i]->value());
            subText->puts(row, column + 2, buf);
        }
    }
}

void displayBallsRemaining(uint16 *screen, int balls)
{
    for (int i = 0; i < 3; i++) {
        int row = (i > 4) ? 20 : 22;
        int col = (i % 5) * 2;
        screen[row * 32 + col] = (i < balls) ? 232 : 0;
        screen[row * 32 + col + 1] = (i < balls) ? 233 : 0;
        screen[(row + 1) * 32 + col] = (i < balls) ? 234 : 0;
        screen[(row + 1) * 32 + col + 1] = (i < balls) ? 235 : 0;
    }
}

void displayScore(uint16 *screen, int score)
{
    uint16* top_left = screen + (20 * 32) + 29;
    
    int working_score = score;
    for (int digit = 0; digit < 5; digit++) {
        int tile_number = 112 + (working_score % 10 * 12);
        
        for (int row = 0; row < 4; row++) {
            for (int column = 0; column < 3; column++) {
                top_left[row * 32 + column] = tile_number++;
            }
        }
        
        top_left -= 3;
        working_score /= 10;
    }
}

void displayStatusScreen(uint16 *screen)
{
    displayLegend(screen);
    displayBallsRemaining(screen, 3);
    displayScore(screen, 0);
}


void initPalette() 
{
      //  dmaCopy(paddlePalette, (uint16*)SPRITE_PALETTE, 512);
    for (int i = 0; i < 256; i++) {
        SPRITE_PALETTE[i] = paletteData[i];
        BG_PALETTE[i] = paletteData[i];
        BG_PALETTE_SUB[i] = paletteData[i];
    }
}

void initPaddle(SpriteEntry *spriteEntry, SpriteRotation *spriteRotation) 
{
    int paddleGfxID = 0;
    
    spriteEntry[0].attribute[0] = ATTR0_DISABLED;
    spriteEntry[0].attribute[1] = ATTR1_ROTDATA(0) | ATTR1_SIZE_16 | SCREEN_HEIGHT - 17;
    spriteEntry[0].attribute[2] = paddleGfxID;
    
    rotateSprite(&spriteRotation[0], 0);
    
    spriteEntry[1].attribute[0] = ATTR0_DISABLED;
    spriteEntry[1].attribute[1] = ATTR1_ROTDATA(0) | ATTR1_SIZE_16 | SCREEN_HEIGHT - 17;
    spriteEntry[1].attribute[2] = paddleGfxID;
    
    rotateSprite(&spriteRotation[1], 0);
    
      //  dmaCopy(paddleData, &SPRITE_GFX[paddleGfxID * 16], 256);
    for (int i = 0; i < (int)(sizeof(paddleData) / sizeof(uint16)); i++)
        SPRITE_GFX[paddleGfxID * 16 + i] = paddleData[i];
}

void initBalls(SpriteEntry *spriteEntry, SpriteRotation *spriteRotation) 
{
    int ballGfxID = 8;
    for (int ball_number = 0; ball_number < MAX_NUMBER_OF_BALLS_IN_PLAY; ball_number++) {
        spriteEntry[2 + ball_number].attribute[0] = ATTR0_DISABLED;
        spriteEntry[2 + ball_number].attribute[1] = ATTR1_ROTDATA(0) | ATTR1_SIZE_16;
        spriteEntry[2 + ball_number].attribute[2] = ballGfxID;
        
        rotateSprite(&spriteRotation[2 + ball_number], 0);
    }
      //  dmaCopy(ballData, &SPRITE_GFX[ballGfxID * 16], 256);
    for (int i = 0; i < (int)(sizeof(ballData) / sizeof(uint16)); i++)
        SPRITE_GFX[ballGfxID * 16 + i] = ballData[i];
}


void initSprites(SpriteEntry *spriteEntry, SpriteRotation *spriteRotation) 
{
    initOAM(spriteEntry, spriteRotation);
    initPaddle(spriteEntry, spriteRotation);
    initBalls(spriteEntry, spriteRotation);
    updateOAM(spriteEntry);
}

void processKeys() 
{
    if (keysHeld() & KEY_LEFT) {
        paddle->moveLeft();
    } else if (keysHeld() & KEY_RIGHT) {
        paddle->moveRight();
    } else {
        paddle->stop();
    }    
}

void enableBall(SpriteEntry *spriteEntry) 
{
    enableSprite(spriteEntry, 2);
}

void enablePaddle(SpriteEntry *spriteEntry) 
{
    enableSprite(spriteEntry, 0);
    enableSprite(spriteEntry, 1);
}

void disableBalls(SpriteEntry *sprites) 
{
    for (int i = 2; i < 6; disableSprite(sprites, i++));
}

void disableSprites(SpriteEntry *sprites) 
{
    disableSprite(sprites, 0);
    disableSprite(sprites, 1);
    disableBalls(sprites);
    updateOAM(sprites);
}

void waitForA(SpriteEntry *sprites)
{
    while(1) {
        scanKeys();
        processKeys();
        Coordinate paddlePosition = paddle->getPosition();
        moveSprite(&sprites[0], paddlePosition.x(), paddlePosition.y());
        moveSprite(&sprites[1], paddlePosition.x() + 16, paddlePosition.y());

        if (keysHeld() & KEY_A) {
            return;
        } else if (first_ball && (current_level < 3) && (keysHeld() & KEY_UP)) {
            board->setupLevel(++current_level);
            while (keysHeld() & KEY_UP) scanKeys();
        } else if (first_ball && (current_level > 0) && (keysHeld() & KEY_DOWN)) {
            board->setupLevel(--current_level);
            while (keysHeld() & KEY_DOWN) scanKeys();
        }
        swiWaitForVBlank();
        updateOAM(sprites);
    }
}

void waitForStart()
{
    while(1) {
        scanKeys();
        processKeys();
        if (keysHeld() & KEY_START) return;
    }
}

void launch(SpriteEntry *sprites)
{
    enablePaddle(sprites);
    disableBalls(sprites);
    waitForA(sprites);
    enableBall(sprites);
    board->launch();
}

int main() 
{
    uint16 *screen = (uint16*)(BG_MAP_RAM(31));
    uint16 *subScreen = (uint16*)(BG_MAP_RAM_SUB(31));

    powerON(POWER_ALL_2D);
    
    irqInit();
    irqSet(IRQ_VBLANK, 0);
    
    initVideo();
    lcdMainOnBottom();
    SpriteEntry *spritesMain = new SpriteEntry[128];
    SpriteRotation *spriteRotationsMain = (SpriteRotation *)spritesMain;
    initSprites(spritesMain, spriteRotationsMain);
    
    mainText = new Text(screen);
    subText = new Text(subScreen);
    paddle = new Paddle();
    board = new Board(paddle, screen, subScreen, spritesMain);
    
    initBackgrounds();
    loadTiles();
    initPalette();
    
    TransferSoundData *game_over_sound = new TransferSoundData();
    game_over_sound->data = gameover_1_11025_8_s_raw;
    game_over_sound->len = gameover_1_11025_8_s_raw_size;
    game_over_sound->rate = 11025;
    game_over_sound->vol = 127;
    game_over_sound->pan = 64;
    game_over_sound->format = 1;
    
    TransferSoundData *start_sound = new TransferSoundData();
    start_sound->data = start_1_11025_8_s_raw;
    start_sound->len = start_1_11025_8_s_raw_size;
    start_sound->rate = 11025;
    start_sound->vol = 127;
    start_sound->pan = 64;
    start_sound->format = 1;

    clearScreen(screen);
    displaySubSplashScreen(subScreen);
    displayMainSplashScreen(screen);
    waitForStart();
    playSound(start_sound);   
    
    
    while (1) {
        board->resetGame();
        displayBackground(screen);
        displayStatusScreen(subScreen);
        current_level = 0;
        board->setupLevel(0);
        first_ball = true;
        
        while(!board->isGameOver()) {
            board->updateBallsRemaining();
            launch(spritesMain);
            first_ball = false;
            
            while(board->isStillInPlay()) {
                scanKeys();
                processKeys();
                Coordinate paddlePosition = paddle->getPosition();
                moveSprite(&spritesMain[0], (int)paddlePosition.x(), (int)paddlePosition.y());
                moveSprite(&spritesMain[1], (int)paddlePosition.x() + 16, (int)paddlePosition.y());
                
                board->moveBalls();
                
                swiWaitForVBlank();
                board->updateScoreDisplay();
                updateOAM(spritesMain);
            }
        }
        
        disableSprites(spritesMain);
        board->clearLevel();
        
        board->displayScreen(brick, subScreen);
        board->updateScoreDisplay();
        
        board->displayGameOver();
        playSound(game_over_sound);
        
        waitForStart();
        playSound(start_sound);   
    }
    
    return 0;
}
