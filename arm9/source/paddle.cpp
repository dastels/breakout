#include "breakout.h"
#include "paddle.h"
#include "vector.h"

Vector movingLeft = Vector(3.14159, 0.25);
Vector movingRight = Vector(0.0, 0.25);
Vector stopped = Vector(0.0, 0.0);

Paddle::Paddle() 
{
  position.x(SCREEN_WIDTH/2 - 8);
  position.y(SCREEN_HEIGHT - 16);
  velocity = &stopped;
}

void Paddle::moveLeft() 
{
    if (position.x() > 8) {        
        position.moveBy(-2, 0);
        velocity = &movingLeft;
    } else {
        stop();
    }
}

void Paddle::moveRight() 
{
    if (position.x() < SCREEN_WIDTH - 41) {
        position.moveBy(2, 0);
        velocity = &movingRight;
    } else {
        stop();
    }
}

void Paddle::stop()
{
    velocity = &stopped;
}


Coordinate Paddle::getPosition() 
{
  return position;
}

void Paddle::addVelocityTo(Vector &v)
{
    v += *velocity;
    v.clipMagnitudeAt(2.0);
    v.clipAngleBetween(0.5, 2.6);
    
}

