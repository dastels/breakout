#include <stdlib.h>

#include "breakout.h"
#include "ball.h"
#include "board.h"

#define WIDTH 8
#define HEIGHT 8
#define COLLISION_MARGIN 1

Ball::Ball(Board *aBoard) 
    : collisionBoundingBox(0, 0, 0, 0)
{
    theBoard = aBoard;
    reset();
}

Ball::Ball(Board *aBoard, int x, int y, Vector initialVelocity) 
    : collisionBoundingBox(0, 0, 0, 0)
{
    theBoard = aBoard;
    position.x(x);
    position.y(y);
    velocity = initialVelocity;
    updateBoundingBox();
}

void Ball::updateBoundingBox()
{
    collisionBoundingBox.top = position.y() + COLLISION_MARGIN;
    collisionBoundingBox.left = position.x() + COLLISION_MARGIN;
    collisionBoundingBox.bottom = position.y() + (HEIGHT - 1) - COLLISION_MARGIN;
    collisionBoundingBox.right = position.x() + (WIDTH - 1) - COLLISION_MARGIN;
}

void Ball::reset() 
{
    position.x(theBoard->paddleX() + 15 - (WIDTH / 2));
    position.y(SCREEN_HEIGHT - (HEIGHT + 3 + 5));
    velocity = Vector((float)(rand() % 160 + 80) / 100 + PI, 1.0);
    updateBoundingBox();
}

Vector Ball::getVelocity()
{
    return velocity;
}

void Ball::addToVelocity(Vector v)
{
    velocity += v;
    velocity.clipMagnitudeBetween(0.5, 2.0);
}

void Ball::subtractFromVelocity(Vector v)
{
    velocity -= v;
}

void Ball::move() 
{
    position.moveBy(velocity);
    position.clipX(0, SCREEN_WIDTH - WIDTH);
    position.clipY(0, SCREEN_HEIGHT);
    updateBoundingBox();
    theBoard->processBall(this);
}

Coordinate Ball::center()
{
    return collisionBoundingBox.center();
}

Coordinate Ball::getPosition() 
{
    return position;
}

Coordinate Ball::convertToTile(Coordinate c)
{
    c.convertToTilePosition();
    return c;
}

void Ball::reflectFromSide(AbstractCell *cell) 
{
    cell->reflectOffVertical(velocity);
}

void Ball::reflectFromTopBottom(AbstractCell *cell) 
{
    cell->reflectOffHorizontal(velocity);
}

Rectangle* Ball::boundingBox()
{
    return &collisionBoundingBox;
}

bool Ball::isCollidingWith(Rectangle *box)
{
    return collisionBoundingBox.collidesWith(box);
}

bool Ball::isCollidingWith(AbstractBall *aBall)
{
    return aBall->isCollidingWith(&collisionBoundingBox);
}

Vector Ball::transferredVelocity()
{
    float angle = ((float)(rand() % 314)) / 100.0 - 157.0;
    float speed = ((float)(rand() % 10)) / 10.0 - 0.5;
    Vector randomized_effect = Vector(angle, speed);
    return velocity + randomized_effect;
}
