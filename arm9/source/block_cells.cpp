#include "block_cells.h"
#include "empty_cell.h"
#include "block1_1_11025_8_s_raw.h"
#include "block2_1_11025_8_s_raw.h"
#include "block3_1_11025_8_s_raw.h"
#include "solidblock_1_11025_8_s_raw.h"
#include "ballblock_1_11025_8_s_raw.h"

BlockCell *blueCell = new BlueBlockCell(0, 0, 0);
BlockCell *greenCell = new GreenBlockCell(0, 0, 0);
BlockCell *redCell = new RedBlockCell(0, 0, 0);
BlockCell *solidCell = new SolidBlockCell(0, 0, 0);
BlockCell *ballCell = new BallBlockCell(0, 0, 0);

BlockCell *samples[] = {redCell, blueCell, greenCell, solidCell, ballCell, 0};


AbstractCell *BlockCell::createCell(Board *theBoard, char type, int row, int column)
{
    switch (type) {
        case 'B': return new BlueBlockCell(theBoard, row, column);
        case 'G': return new GreenBlockCell(theBoard, row, column);
        case 'R': return new RedBlockCell(theBoard, row, column);
        case 'S': return new SolidBlockCell(theBoard, row, column);
        case 'o': return new BallBlockCell(theBoard, row, column);
        default: return new EmptyCell();
    }
}


int BlockCell::codeToTile(char code)
{
    switch (code) {
        case 'B': return blueCell->tileNumber();
        case 'G': return greenCell->tileNumber();
        case 'R': return redCell->tileNumber();
        case 'S': return solidCell->tileNumber();
        case 'o': return ballCell->tileNumber();
        default: return 0;
    }
}


BlockCell** BlockCell::sampleBlocks()
{
    return samples;
}

BlockCell::BlockCell(Board *theBoard, int row, int column)
    : Cell(theBoard, column, row), bounds(row*8, column*8, row*8+7, column*8+7)
{
}

int BlockCell::value()
{
    return 0;
}

int BlockCell::tileNumber()
{
    return 0;
}

TransferSoundData *BlockCell::sound()
{
    return 0;
}

bool BlockCell::removable()
{
    return true;
}

void BlockCell::processHit(AbstractBall *b) 
{
    Cell::processHit(b);
    if (sound()) playSound(sound());
    if (removable()) board->removeBlock(this);
    board->addToScore(value());
}

Rectangle* BlockCell::boundingBox()
{
  return &bounds;
}

//----------------------------------------

TransferSoundData *BlueBlockCell::sound_data = 0;

BlueBlockCell::BlueBlockCell(Board* theBoard, int row, int column)
    :BlockCell(theBoard, row, column)
{
    if (sound_data) return;
    
    sound_data = new TransferSoundData();
    sound_data->data = block1_1_11025_8_s_raw;
    sound_data->len = block1_1_11025_8_s_raw_size;
    sound_data->rate = 11025;
    sound_data->vol = 127;
    sound_data->pan = 64;
    sound_data->format = 1;
}

TransferSoundData *BlueBlockCell::sound()
{
    return sound_data;
}

int BlueBlockCell::tileNumber()
{
    return 8;
}

int BlueBlockCell::value()
{
    return 5;
}

  //----------------------------------------

TransferSoundData *RedBlockCell::sound_data = 0;

RedBlockCell::RedBlockCell(Board* theBoard, int row, int column)
    :BlockCell(theBoard, row, column)
{
    if (sound_data) return;
    
    sound_data = new TransferSoundData();
    sound_data->data = block2_1_11025_8_s_raw;
    sound_data->len = block2_1_11025_8_s_raw_size;
    sound_data->rate = 11025;
    sound_data->vol = 127;
    sound_data->pan = 64;
    sound_data->format = 1;
}

int RedBlockCell::tileNumber()
{
    return 6;
}

int RedBlockCell::value()
{
    return 1;
}

TransferSoundData *RedBlockCell::sound()
{
    return sound_data;
}

//----------------------------------------

TransferSoundData *GreenBlockCell::sound_data = 0;

GreenBlockCell::GreenBlockCell(Board* theBoard, int row, int column)
    :BlockCell(theBoard, row, column)
{
    if (sound_data) return;
    
    sound_data = new TransferSoundData();
    sound_data->data = block3_1_11025_8_s_raw;
    sound_data->len = block3_1_11025_8_s_raw_size;
    sound_data->rate = 11025;
    sound_data->vol = 127;
    sound_data->pan = 64;
    sound_data->format = 1;
}

TransferSoundData *GreenBlockCell::sound()
{
    return sound_data;
}

int GreenBlockCell::tileNumber()
{
    return 7;
}

int GreenBlockCell::value()
{
    return 10;
}

//----------------------------------------

TransferSoundData *SolidBlockCell::sound_data = 0;

SolidBlockCell::SolidBlockCell(Board *theBoard, int row, int column)
    :BlockCell(theBoard, row, column)
{
    if (sound_data) return;
    
    sound_data = new TransferSoundData();
    sound_data->data = solidblock_1_11025_8_s_raw;
    sound_data->len = solidblock_1_11025_8_s_raw_size;
    sound_data->rate = 11025;
    sound_data->vol = 127;
    sound_data->pan = 64;
    sound_data->format = 1;
}

TransferSoundData *SolidBlockCell::sound()
{
    return sound_data;
}

int SolidBlockCell::tileNumber()
{
    return 9;
}

int SolidBlockCell::value()
{
    return 0;
}

bool SolidBlockCell::removable()
{
    return false;
}

//----------------------------------------

TransferSoundData *BallBlockCell::sound_data = 0;

BallBlockCell::BallBlockCell(Board *theBoard, int row, int column)
    :BlockCell(theBoard, row, column)
{
    if (sound_data) return;
    
    sound_data = new TransferSoundData();
    sound_data->data = ballblock_1_11025_8_s_raw;
    sound_data->len = ballblock_1_11025_8_s_raw_size;
    sound_data->rate = 11025;
    sound_data->vol = 127;
    sound_data->pan = 64;
    sound_data->format = 1;
}

int BallBlockCell::tileNumber() 
{
    return 10;
}

int BallBlockCell::value()
{
    return board->canLaunchAnotherBall() ? 20 : 0;
}

TransferSoundData *BallBlockCell::sound()
{
    return board->canLaunchAnotherBall() ? sound_data : 0;
}

void BallBlockCell::processHit(AbstractBall *b)
{
    BlockCell::processHit(b);
    board->addBall(row, column, b->transferredVelocity());
}

bool BallBlockCell::removable()
{
    return board->canLaunchAnotherBall();
}

