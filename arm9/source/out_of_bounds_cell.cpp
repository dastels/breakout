#include "out_of_bounds_cell.h"

Rectangle OutOfBoundsCell::bounds = Rectangle(SCREEN_HEIGHT, 0, SCREEN_HEIGHT + 8, SCREEN_WIDTH - 1);

OutOfBoundsCell::OutOfBoundsCell(Board *theBoard)
    : Cell(theBoard, 0, 0)
{
}

Rectangle *OutOfBoundsCell::boundingBox()
{
    return &bounds;
}

void OutOfBoundsCell::processHit(AbstractBall *b)
{
    Cell::processHit(b);
    board->wentOutOfBounds(b);
}
