#include "breakout.h"
#include "rectangle.h"

Coordinate Rectangle:: coord;

Rectangle *Rectangle::create(int t, int l, int b, int r)
{
  if ((t > b) || (l > r))
    return new EmptyRectangle();
  else
    return new Rectangle(t, l, b, r);
}


Rectangle::Rectangle(int t, int l, int b, int r)
{
  top = t;
  left = l;
  bottom = b;
  right = r;
}

Rectangle::~Rectangle()
{
}

bool Rectangle::isEmpty()
{
  return (top > bottom) || (left > right);
}

Rectangle *Rectangle::operator|(Rectangle *r)
{
  return Rectangle::create(min(top, r->top), min(left, r->left), max(bottom, r->bottom), max(right, r->right));
}

Rectangle *Rectangle::operator&(Rectangle *r)
{
  return Rectangle::create(max(top, r->top), max(left, r->left), min(bottom, r->bottom), min(right, r->right));
}

Rectangle *Rectangle::inset(int margin)
{
  return Rectangle::create(top + margin, left + margin, bottom - margin, right - margin);
}

bool Rectangle::collidesWith(Rectangle *r)
{
  if (top > r->bottom) return false;
  if (bottom < r->top) return false;
  if (left > r->right) return false;
  if (right < r->left) return false;
  return true;
}

Coordinate Rectangle::bottomLeft()
{
  coord.x(left);
  coord.y(bottom);
  return coord;
}

Coordinate Rectangle::bottomCenter()
{
  coord.x((left + right) / 2);
  coord.y(bottom);
  return coord;
}

Coordinate Rectangle::bottomRight()
{
  coord.x(right);
  coord.y(bottom);
  return coord;
}

Coordinate Rectangle::topLeft()
{
  coord.x(left);
  coord.y(top);
  return coord;
}

Coordinate Rectangle::topCenter()
{
  coord.x((left + right) / 2);
  coord.y(top);
  return coord;
}

Coordinate Rectangle::topRight()
{
  coord.x(right);
  coord.y(top);
  return coord;
}

Coordinate Rectangle::middleLeft()
{
  coord.x(left);
  coord.y((top + bottom) / 2);
  return coord;
}

Coordinate Rectangle::middleRight()
{
  coord.x(right);
  coord.y((top + bottom) / 2);
  return coord;
}

Coordinate Rectangle::center()
{
    return Coordinate((right + left) / 2, (bottom + top) / 2);
}

// EmptyRectangle

EmptyRectangle::EmptyRectangle()
  :Rectangle(0, 0, 0, 0)
{
}

bool EmptyRectangle::isEmpty()
{
  return true;
}

Rectangle *EmptyRectangle::operator|(Rectangle *r)
{
  return r;
}

Rectangle *EmptyRectangle::operator&(Rectangle *r)
{
  return this;
}

Rectangle *EmptyRectangle::inset(int i)
{
  return this;
}

bool EmptyRectangle::collidesWith(Rectangle *r)
{
  return false;
}

