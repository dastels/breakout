#include <math.h>
#include "coordinate.h"

Coordinate::Coordinate(float x, float y)
{
    xVal = x;
    yVal = y;
}

Coordinate::Coordinate(int x, int y)
{
    xVal = (float)x;
    yVal = (float)y;
}

int Coordinate::x() 
{
  return (int)xVal;
}

int Coordinate::y() 
{
  return (int)yVal;
}

void Coordinate::x(int val) 
{
  xVal = (float)val;
}

void Coordinate::y(int val) 
{
  yVal = (float)val;
}

void Coordinate::x(float val) 
{
  xVal = val;
}

void Coordinate::y(float val) 
{
  yVal = val;
}

void Coordinate::moveBy(int dx, int dy) 
{
  xVal += (float)dx;
  yVal += (float)dy;
}

void Coordinate::moveBy(float dx, float dy) 
{
  xVal += dx;
  yVal += dy;
}

void Coordinate::moveBy(Vector v) 
{
    xVal += v.x();
    yVal += v.y();
}

void Coordinate::convertToTilePosition()
{
    x((int)(x() / 8));
    y((int)(y() / 8));
}

void Coordinate::clipX(int min, int max) 
{
  xVal = clip(xVal, (float)min, (float)max);
}

void Coordinate::clipY(int min, int max) 
{
  yVal = clip(yVal, min, max);
}

float Coordinate::clip(float val, float min, float max) 
{
  if (val < min) return min;
  if (val > max) return max;
  return val;
}

bool Coordinate::outOfBoundsX(int min, int max) 
{
  return outOfBounds(xVal, (float)min, (float)max);
}

bool Coordinate::outOfBoundsY(int min, int max) 
{
  return outOfBounds(yVal, (float)min, (float)max);
}

bool Coordinate::outOfBounds(float val, float min, float max) 
{
  if (val < min) return true;
  if (val > max) return true;
  return false;
}

Vector Coordinate::vectorDifference(Coordinate other)
{
    float dx = xVal - other.xVal;
    float dy = yVal - other.yVal;

    return Vector(atan2(dy, dx), sqrt(dx*dx + dy*dy));
}
