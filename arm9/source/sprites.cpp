#include "nds.h"
#include "sprites.h"

void updateOAM(SpriteEntry *spriteEntry) 
{
  DC_FlushAll();
  unsigned short *temp = (unsigned short *)spriteEntry;
  int oamSize = 128 * sizeof(SpriteEntry);

  //  dmaCopy(spriteEntry, OAM, 128 * sizeof(SpriteEntry));
  for (int i = 0; i < oamSize; i++)
    OAM[i] = temp[i];
}

void initOAM(SpriteEntry *spriteEntry, SpriteRotation *spriteRotation) 
{
  for (int i = 0; i < 128; i++) {
    spriteEntry[i].attribute[0] = ATTR0_DISABLED;
    spriteEntry[i].attribute[1] = 0;
    spriteEntry[i].attribute[2] = 0;
  }
  
  for (int i = 0; i < 32; i++) {
    spriteRotation[i].hdx = 256;
    spriteRotation[i].hdy = 0;
    spriteRotation[i].vdx = 0;
    spriteRotation[i].vdy = 256;
  }

  updateOAM(spriteEntry);
}

void moveSprite(SpriteEntry *spriteEntry, u16 x, u16 y) 
{
  spriteEntry->attribute[0] &= 0xFF00;
  spriteEntry->attribute[0] |= (y & 0x00FF);

  spriteEntry->attribute[1] &= 0xFE00;
  spriteEntry->attribute[1] |= (x & 0X01FF);
}

void rotateSprite(SpriteRotation *spriteRotation, u16 angle) 
{
  s16 s = -SIN[angle & 0x1FF] >> 4;
  s16 c = COS[angle & 0x1FF] >> 4;

  spriteRotation->hdx = c;
  spriteRotation->hdy = -s;
  spriteRotation->vdx = s;
  spriteRotation->vdy = c;
}

void enableSprite(SpriteEntry *sprites, int spriteToEnable)
{
    sprites[spriteToEnable].attribute[0] = ATTR0_COLOR_256 | ATTR0_NORMAL;
}

void disableSprite(SpriteEntry *sprites, int spriteToDisable)
{
    sprites[spriteToDisable].attribute[0] = ATTR0_DISABLED;
    updateOAM(sprites);
}

