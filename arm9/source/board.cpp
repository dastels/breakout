#include <stdio.h>
#include <nds.h>

#include "ball.h"
#include "sprites.h"
#include "breakout.h"
#include "board.h"
#include "text.h"
#include "empty_cell.h"
#include "wall_cells.h"
#include "paddle_cell.h"
#include "block_cells.h"
#include "out_of_bounds_cell.h"
#include "levels.h"
#include "game_over.h"


Board::Board(Paddle *aPaddle, uint16 *screenRam, uint16 *subScreenRam, SpriteEntry *spriteEntry)
    :textWriter(screenRam)
{
    screen = screenRam;
    subScreen = subScreenRam;
    sprites = spriteEntry;
    paddle = aPaddle;
    for (int row = 1; row < 23; row++)
        for (int column = 1; column < 31; column++)
            cells[row][column] = new EmptyCell();
    addBorder();
    addPaddle(paddle);
    addOutOfBounds();
    resetGame();
}

void Board::resetGame()
{
    score = 0;
    inPlay = false;
    ballsRemaining = 3;
    number_of_balls = 1;
    for (int i = 0; i < MAX_NUMBER_OF_BALLS_IN_PLAY; balls[i++] = 0);
}

AbstractCell* Board::cellAt(int row, int column) 
{
    return cells[row][column];
}

AbstractCell* Board::cellAt(Coordinate position) 
{
    return cellAt(position.y(), position.x());
}

void Board::addBorder() 
{
    TopWallCell *top = new TopWallCell();
    SideWallCell *left = new SideWallCell(false);
    SideWallCell *right = new SideWallCell(true);
    for (int column = 0; column < 32; column++) {
        cells[0][column] = top;
    }
    for (int row = 0; row < 24; row++) {
        cells[row][0] = left;
        cells[row][31] = right;
    }
}

void Board::addPaddle(Paddle *paddle) 
{
    PaddleCell *cell = new PaddleCell(this, paddle);
    for (int column = 1; column < 31; column++)
        cells[23][column] = cell;
}

void Board::addOutOfBounds() 
{
    OutOfBoundsCell *cell = new OutOfBoundsCell(this);
    for (int column = 0; column < 32; column++)
        cells[24][column] = cell;
}

void Board::addBlocks()
{
    setupLevel(0);
}

void Board::setupLevel(int levelNumber)
{
    for (int row = 0; row < 20; row++) {
        char *line = levels[levelNumber * 20 + row];
        for (int column = 0; column < 30; column++) {
            AbstractCell *theCell = BlockCell::createCell(this, *line++, row + 1, column + 1);
            cells[row + 1][column + 1] = theCell;
            screen[row * 32 + column + 33] = theCell->tileNumber();
        }
    }
}

void Board::clearLevel()
{
    for (int row = 1; row < 22; row++) {
        for (int column = 1; column < 31; column++) {
            AbstractCell *cell = cells[row][column];
            if (cell) delete cell;
            cells[row][column] = (AbstractCell*)0;
        }
    }
}

void Board::displayScreen(char* data[], uint16 *destination)
{
    for (int row = 0; row < 24; row++) {
        char *line = data[row];
        for (int column = 0; column < 32; column++) {
            destination[row * 32 + column] = BlockCell::codeToTile(line[column]);
        }
    }
}

void Board::displayGameOver()
{
    displayScreen(gameOver, screen);
    textWriter.puts(21, 3, "Press START for a new game");
}

void Board::processBall(AbstractBall *aBall) 
{
     checkForAndHandleCollision(aBall);
}

void Board::checkForAndHandleCollision(AbstractBall *aBall)
{
    checkForAndHandleBallCollision(aBall);
    checkForAndHandleNonCornerCollision(aBall) || checkForAndHandleCornerCollision(aBall);
}

bool Board::checkForAndHandleBallCollision(AbstractBall *ball1)
{
    for (int ball_number = 0; ball_number < MAX_NUMBER_OF_BALLS_IN_PLAY; ball_number++) {
        AbstractBall *ball2 = balls[ball_number];
        if (!ball2) continue;
        if (ball2 == ball1) continue;

        Vector n12 = ball1->center().vectorDifference(ball2->center());

        if (n12.magnitude() > 8.0) continue; // not touching
        if (ball1->getVelocity().isInTheSameDirectionAs(n12)) continue;
        
        n12.normalize();
        Vector projection_onto_n12_of_v1 = n12 * n12.dot(ball1->getVelocity());

        Vector n21 = ball2->center().vectorDifference(ball1->center());
        n21.normalize();
        Vector projection_onto_n21_of_v2 = n21 * n21.dot(ball2->getVelocity());
        
        ball1->addToVelocity(projection_onto_n21_of_v2 - projection_onto_n12_of_v1);
        ball2->addToVelocity(projection_onto_n12_of_v1 - projection_onto_n21_of_v2);  
        
        return true;
    }
    return false;
}

bool Board::checkForAndHandleCornerCollision(AbstractBall *aBall)
{
    int top = aBall->getTopCellPosition().y();
    int bottom = aBall->getBottomCellPosition().y();
    int left = aBall->getLeftCellPosition().x();
    int right = aBall->getRightCellPosition().x();

    if (aBall->isHeadingPrimarilyUp()) {
        if (aBall->isHeadingLeft()) verticalHit(cellAt(top, left), aBall);
        if (aBall->isHeadingRight()) verticalHit(cellAt(top, right), aBall);
    }
    if (aBall->isHeadingPrimarilyDown()) {
        if (aBall->isHeadingLeft()) verticalHit(cellAt(bottom, left), aBall);
        if (aBall->isHeadingRight()) verticalHit(cellAt(bottom, right), aBall);
    }
    if (aBall->isHeadingPrimarilyLeft()) {
        if (aBall->isHeadingUp()) horizontalHit(cellAt(top, left), aBall);
        if (aBall->isHeadingDown()) horizontalHit(cellAt(bottom, left), aBall);
    }
    if (aBall->isHeadingPrimarilyRight()) {
        if (aBall->isHeadingUp()) horizontalHit(cellAt(top, right), aBall);
        if (aBall->isHeadingDown()) horizontalHit(cellAt(bottom, right), aBall);
    }

    return false;
}

bool Board::verticalHit(AbstractCell *aCell, AbstractBall *aBall)
{
    if (!aCell->isVertical()) return false;
    if (!aCell->isHitBy(aBall)) return false;
    aCell->processHit(aBall);
    aBall->reflectFromSide(aCell);
    return true;    
}

bool Board::horizontalHit(AbstractCell *aCell, AbstractBall *aBall)
{
    if (!aCell->isHorizontal()) return false;
    if (!aCell->isHitBy(aBall)) return false;
    aCell->processHit(aBall);
    aBall->reflectFromTopBottom(aCell);
    return true;    
}

bool Board::checkForAndHandleNonCornerCollision(AbstractBall *aBall)
{
    if (checkForAndHandleBottomHit(cellAt(aBall->getBottomCellPosition()), aBall)) return true;
    if (checkForAndHandleTopHit(cellAt(aBall->getTopCellPosition()), aBall)) return true;
    if (checkForAndHandleLeftHit(cellAt(aBall->getLeftCellPosition()), aBall)) return true;
    if (checkForAndHandleRightHit(cellAt(aBall->getRightCellPosition()), aBall)) return true;
    return false;
}

bool Board::checkForAndHandleLeftHit(AbstractCell *aCell, AbstractBall *aBall) 
{
    if (!aCell->isVertical()) return false;
    if (!aCell->isHitBy(aBall)) return false;
    if (aBall->isHeadingRight()) return false;
    aCell->processHit(aBall);
    aBall->reflectFromSide(aCell);
    return true;
}

bool Board::checkForAndHandleRightHit(AbstractCell *aCell, AbstractBall *aBall) 
{
    if (!aCell->isVertical()) return false;
    if (!aCell->isHitBy(aBall)) return false;
    if (aBall->isHeadingLeft()) return false;
    aCell->processHit(aBall);
    aBall->reflectFromSide(aCell);
    return true;
}

bool Board::checkForAndHandleTopHit(AbstractCell *aCell, AbstractBall *aBall) 
{
    if (!aCell->isHorizontal()) return false;
    if (!aCell->isHitBy(aBall)) return false;
    if (aBall->isHeadingDown()) return false;
    aCell->processHit(aBall);
    aBall->reflectFromTopBottom(aCell);
    return true;
}

bool Board::checkForAndHandleBottomHit(AbstractCell *aCell, AbstractBall *aBall) 
{
    if (!aCell->isHorizontal()) return false;
    if (!aCell->isHitBy(aBall)) return false;
    if (aBall->isHeadingUp()) return false;
    aCell->processHit(aBall);
    aBall->reflectFromTopBottom(aCell);
    return true;
}

void Board::addToScore(int value)
{
    score += value;
}

void Board::updateScoreDisplay()
{
    displayScore(subScreen, score);
}

void Board::updateBallsRemaining()
{
    displayBallsRemaining(subScreen, ballsRemaining);
}

int Board::paddleX()
{
    return paddle->getPosition().x();
}

void Board::wentOutOfBounds(AbstractBall *aBall)
{
    int ball_number = 0;
    for (ball_number = 0; ball_number < MAX_NUMBER_OF_BALLS_IN_PLAY && (balls[ball_number] != aBall); ball_number++);
    
    if (ball_number == MAX_NUMBER_OF_BALLS_IN_PLAY) return;

    disableSprite(sprites, 2 + ball_number);
    balls[ball_number] = 0;
    if (--number_of_balls == 0) {
        ballsRemaining--;
        inPlay = false;
    }
    
}

bool Board::isStillInPlay()
{
    return inPlay;
}

bool Board::isGameOver()
{
    return ballsRemaining == 0;
}

void Board::clearBalls()
{
    for (int i = 0; i < MAX_NUMBER_OF_BALLS_IN_PLAY; i++) {
        if (balls[i]) delete balls[i];
        balls[i] = 0;
    }
    number_of_balls = 0;
}

void Board::removeBlock(AbstractCell *aCell)
{
    cells[aCell->getRow()][aCell->getColumn()] = new EmptyCell();
    screen[aCell->getRow() * 32 + aCell->getColumn()] = 0x0000;
}

void Board::moveBalls()
{
    for (int ball_number = 0; ball_number < MAX_NUMBER_OF_BALLS_IN_PLAY; ball_number++) {
        if (balls[ball_number]) {
            AbstractBall *ball = balls[ball_number];
            ball->move();
            Coordinate ballPosition = ball->getPosition();
            moveSprite(&sprites[ball_number + 2], (int)ballPosition.x(), (int)ballPosition.y());
        }
    }
}

void Board::launch() 
{
    clearBalls();
    addAndEnableBall(new Ball(this));
    inPlay = true;
}

void Board::addBall(int row, int column, Vector initialVelocity)
{
    addAndEnableBall(new Ball(this, column * 8, row * 8, initialVelocity));
}

void Board::addAndEnableBall(AbstractBall *aBall)
{
    int ball_number = 0;
    for (ball_number = 0; ball_number < MAX_NUMBER_OF_BALLS_IN_PLAY && balls[ball_number]; ball_number++);
    if (ball_number == MAX_NUMBER_OF_BALLS_IN_PLAY) return;

    balls[ball_number] = aBall;
    Coordinate ballPosition = aBall->getPosition();
    moveSprite(&sprites[ball_number + 2], (int)ballPosition.x(), (int)ballPosition.y());
    enableSprite(sprites, ball_number + 2);
    number_of_balls++;
}    

bool Board::canLaunchAnotherBall()
{
    return number_of_balls < MAX_NUMBER_OF_BALLS_IN_PLAY;
}
