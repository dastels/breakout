#include "wall_cells.h"

WallCell::WallCell(int t, int l, int b, int r)
  :Cell(0, 0, 0), bounds(t, l, b, r)
{
}

Rectangle *WallCell::boundingBox()
{
  return &bounds;
}

//----------------------------------------

SideWallCell::SideWallCell(bool onRight) 
  :WallCell(0, (onRight ? SCREEN_WIDTH - 8 : 0), SCREEN_HEIGHT - 1, (onRight ? SCREEN_WIDTH - 1 : 7))
{
}

bool SideWallCell::isHorizontal()
{
    return false;
}

//----------------------------------------

TopWallCell::TopWallCell() 
  :WallCell(0, 0, 7, SCREEN_WIDTH - 1)
{
}

bool TopWallCell::isVertical()
{
    return false;
}

