#include "empty_cell.h"

EmptyCell::EmptyCell()
    :Cell(0, -1, -1)
{
    emptyBox = Rectangle::create(0, 0, -1, -1);
}

bool EmptyCell::isHitBy(AbstractBall *b) 
{
    return false; 
}

Rectangle* EmptyCell::boundingBox()
{
    return emptyBox;
}
