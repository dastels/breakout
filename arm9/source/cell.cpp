#include "cell.h"

Cell::Cell(Board *theBoard, int c, int r) 
{
  board = theBoard;
  row = r;
  column = c;
}

int Cell::getRow() 
{
  return row; 
}

int Cell::getColumn() 
{ 
  return column; 
}

int Cell::tileNumber()
{
  return 0;
}

bool Cell::isHitBy(AbstractBall *b) 
{
    return b->isCollidingWith(boundingBox());
}

void Cell::processHit(AbstractBall *b)
{
}

bool Cell::isVertical()
{
    return true;
}

void Cell::reflectOffVertical(Vector &v)
{
    v.flipX();
}

bool Cell::isHorizontal()
{
    return true;
}

void Cell::reflectOffHorizontal(Vector &v)
{
    v.flipY();
}

Cell::~Cell() 
{
}

